<?php

namespace Drupal\views_access_own_account\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\ResettableStackedRouteMatchInterface;

/**
 * Checks access for displaying configuration translation page.
 */
class CustomAccessCheck implements AccessInterface {

  /**
   * Service current_route_match.
   *
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * CustomAccessCheck constructor.
   *
   * @param \Drupal\Core\Routing\ResettableStackedRouteMatchInterface $current_route_match
   *   Service current_route_match.
   */
  public function __construct(ResettableStackedRouteMatchInterface $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $user_view_own_account = ($account->id() == $this->currentRouteMatch->getRawParameter('user'));
    return $user_view_own_account ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
