<?php

namespace Drupal\views_access_own_account\Plugin\views\access;

use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides access to own user profile.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "access_own_account",
 *   title = @Translation("Access own account"),
 *   help = @Translation("Will be available if the ID of a current user is the same as the user's ID that is the part of the view's path.")
 * )
 */
class AccessOwnAccount extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('Access own account');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    // No access control.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_custom_access', 'views_access_own_account.access_checker::access');
  }

}
